#!/bin/bash

# Check if wireguard interface is up
if sudo wg show | grep -q "vpn" >& /dev/null; then
    # If wireguard interface is up, stop it
    sudo wg-quick down vpn >& /dev/null
    echo "VPN is now disabled."
else
    # If wireguard interface is down, start it
    sudo wg-quick up vpn >& /dev/null
    echo "VPN is now enabled."
fi
